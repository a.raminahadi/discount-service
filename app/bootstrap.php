<?php

require_once '../vendor/autoload.php';

$container = new \Slim\Container;
$app = new \App\App($container);

require_once 'routes.php';