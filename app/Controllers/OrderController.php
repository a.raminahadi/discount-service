<?php

namespace App\Controllers;

class OrderController extends Controller {

	public function post(){
		$payload = $this->request->getParsedBody();
		$total = $payload['total'];
		$percentage = 10;

		if ($total > 1000) {
			$discount = $this->calculate_percentage($total, $percentage);
			$total_payment = $total - $discount;

			return $this->response("You got 10% discount on this order because of purchasing over 1000 euro. Now the total payment is " . $total_payment . " euro", 200);
		}

		else {
			
			$results = [];

		
			$items = $payload['items'];
			foreach ($items as $item) {
				$product_id 	  = $item['product-id'];
				$product_category = $this->get_product_category_by_id($product_id);

				

				switch ($product_category) {
					case '1':
						$product_quantity = $item['quantity'];
						if ($product_quantity >= 2) {
							$cheapest_product = $this->get_cheapest_product('1');

							if ($cheapest_product['id'] === $item['product-id']) {
								$discount = $this->calculate_percentage($cheapest_product['price'] , 20);
								$total_product_price = $item['total'] - $discount;
							} else {
								$total_product_price = $item['total'];
							}

							$results [] = [
								"Product_id" => $item['product-id'],
								"Quantity"   => $product_quantity,
								"Total"      => $total_product_price, 
								"Message"    => "you got a 20% discount of our cheapest product on buying more than 2 products of this category type"
							];

						} else {
							$results [] = [
								"Product_id" => $item['product-id'],
								"Quantity" 	 => $product_quantity,
								"Total"		 => $item['total'],
								"Message"    => "No discount for this product"

							];
						}
						break;

					case '2':
						$product_quantity = $item['quantity'];

						if ($product_quantity >= 5) {
							$product_new_quantity = $product_quantity + 1;

							$results [] = [
								"Product_id"   	   => $item['product-id'],
								"Ordered Quantity" => $product_quantity,
								"New Quantity" 	   => $product_new_quantity,
								"Total" 		   => $item['total'],
								"Message" 		   => "You got one extra product free because of purchasing 5 products of this category type"
							];
						} else {	
							$results [] = [
								"Product_id"   	   => $item['product-id'],
								"Ordered Quantity" => $product_quantity,
								"Total" 		   => $item['total'],
								"Message" 		   => "No discount on this product"
							];
						}			

						break;
					default:
						break;
				}

			}

		
			return json_encode($results);
		}

		
		


	}

	protected function calculate_percentage($value, $percentage){
		$discount = ($value * $percentage) / 100;
		return $discount;
	}


	protected function get_product_category_by_id($product_id){
		$products = $this->get_products_data_from_file();
		foreach ($products as $product) {
			if ($product['id'] === $product_id) {
				return $product['category'];
			}
		}
	}

	protected function get_products_data_from_file(){
		$products = file_get_contents("http://localhost/Discount-Service/Products.json");
		$products = json_decode($products,true);
		return $products;	
	}


	protected function get_cheapest_product($product_category){
		$products = $this->get_products_data_from_file();
		$products_matched = [];
		foreach ($products as $product) {
			if ($product['category'] === $product_category) {
				$products_matched[] = $product;
			}
		}
		uasort($products_matched, function($a, $b){
			return $a['price'] - $b['price'];
		});

		return $products_matched[0];
	}
}
?>
