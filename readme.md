# Problem : Discounts

This is a small (micro)service that calculates discounts for orders.

## How discounts work

For now, there are three possible ways of getting a discount:

- A customer who has already bought for over € 1000, gets a discount of 10% on the whole order.
- For every product of category "Switches" (id 2), when you buy five, you get a sixth for free.
- If you buy two or more products of category "Tools" (id 1), you get a 20% discount on the cheapest product.


## APIs

In the [example-orders](./example-orders/) directory, you can find a couple of example orders.
The service accepts data in this form.

The source file for product data is in Products.json file.


## Instructions

- Download and locate the project inside the directory root of your web server
- run the command "composer install" to install Slim
- The API is ready to be served ...

Example:

Open your REST client (such as POSTMAN) and type in the URL http://localhost/discount-service/public/orders, select POST as http method and pass the data in body section.




